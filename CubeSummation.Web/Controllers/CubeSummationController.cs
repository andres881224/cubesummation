﻿using CubeSummation.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace CubeSummation.Web.Controllers
{
    public class CubeSummationController : Controller
    {
        public ActionResult Index()
        { 
            return View();
        }

        public JsonResult Proceso(string texto)
        { 
            return Json(CalculoCubo.CalcularDatos(texto));
        }

    }
}