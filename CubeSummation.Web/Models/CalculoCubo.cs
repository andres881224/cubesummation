﻿using System;

namespace CubeSummation.Web.Models
{
    public static class CalculoCubo
    {
        static int T, N, M;
        static int[,,] Matriz3D;

        public static string CalcularDatos(string secuencia)
        {
            try
            {
                string resultado = string.Empty;

                int posSecuencia = 0;
                string[] listaSecuencia = secuencia.Split("\n");
                T = Convert.ToInt32(listaSecuencia[posSecuencia]);

                for (int i = 0; i < T; i++)
                {
                    posSecuencia += 1;
                    string[] NMLineParts = listaSecuencia[posSecuencia].Trim().Split(" ");
                    N = Convert.ToInt32(NMLineParts[0]);
                    M = Convert.ToInt32(NMLineParts[1]);
                    Matriz3D = new int[N + 1, N + 1, N + 1];

                    for (int j = 0; j < M; j++)
                    {
                        posSecuencia = posSecuencia + 1;
                        string Operacion = listaSecuencia[posSecuencia];

                        string[] OperacionLineParts = Operacion.Split(" ");
                        if (OperacionLineParts[0].Equals("UPDATE"))
                        {
                            Actualizar(Convert.ToInt32(OperacionLineParts[1]), Convert.ToInt32(OperacionLineParts[2]), Convert.ToInt32(OperacionLineParts[3]), Convert.ToInt32(OperacionLineParts[4]));
                        }
                        if (OperacionLineParts[0].Equals("QUERY"))
                        {
                            resultado = resultado +
                                Consulta(Convert.ToInt32(OperacionLineParts[1]), Convert.ToInt32(OperacionLineParts[2]), Convert.ToInt32(OperacionLineParts[3]), Convert.ToInt32(OperacionLineParts[4]), Convert.ToInt32(OperacionLineParts[5]), Convert.ToInt32(OperacionLineParts[6]))
                                + "\n";
                        }
                    }
                }
                return resultado;
            }
            catch
            {
                return "Varificar texto de entrada.";
            }            
        }

        private static void Actualizar(int x, int y, int z, int valor)
        {
            Matriz3D[x, y, z] = valor;
        }

        private static string Consulta(int x1, int y1, int z1, int x2, int y2, int z2)
        {
            int sum = 0;
            for (int x = 0; x <= N; x++)
            {
                for (int y = 0; y <= N; y++)
                {
                    for (int z = 0; z <= N; z++)
                    {
                        if ((x >= x1 && y >= y1 && z >= z1) &&
                            (x <= x2 && y <= y2 && z <= z2))
                        {
                            sum += Matriz3D[x, y, z];
                        }
                    }
                }
            }
            return sum.ToString();
        }



    }
}
