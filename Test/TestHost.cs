﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Test
{
    [TestClass]
    public class TestHost : IDisposable
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public TestHost()
        {
            var builder = new WebHostBuilder().UseStartup<CubeSummation.Web.Startup>();
            _server = new TestServer(builder);
            _client = _server.CreateClient();
            _client.BaseAddress = new Uri("http://localhost:8888");
        }
        public void Dispose()
        {
            _client.Dispose();
            _server.Dispose();
        }

        [TestMethod]
        public async Task CargarIndex()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "/Index");

            var response = await _client.SendAsync(request);

            Assert.IsTrue((int)response.StatusCode == 200);
        }

    }
}