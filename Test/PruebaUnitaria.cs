using CubeSummation.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class PruebaUnitaria
    {
        [TestMethod]
        public void ProcesoTestOk()
        {
            string texto = "2\n" +
                "4 5\n" +
                "UPDATE 2 2 2 4\n" +
                "QUERY 1 1 1 3 3 3\n" +
                "UPDATE 1 1 1 23\n" +
                "QUERY 2 2 2 4 4 4\n" +
                "QUERY 1 1 1 3 3 3\n" +
                "2 4\n" +
                "UPDATE 2 2 2 1\n" +
                "QUERY 1 1 1 1 1 1\n" +
                "QUERY 1 1 1 2 2 2\n" +
                "QUERY 2 2 2 2 2 2";
            var resultadoEsperado = "4\n4\n27\n0\n1\n1\n";
            var resuultado = CalculoCubo.CalcularDatos(texto);
            Assert.AreEqual(resultadoEsperado, resuultado);
        }

        [TestMethod]
        public void ProcesoTestErrorDiferenteResultado()
        {
            string texto = "1\n" +
                "4 5\n" +
                "UPDATE 2 2 2 4\n" +
                "QUERY 1 1 1 3 3 3\n" +
                "UPDATE 1 1 1 23\n" +
                "QUERY 2 2 2 4 4 4\n" +
                "QUERY 1 1 1 3 3 3";

            var resultadoEsperado = "4\n4\n27\n0\n1\n1\n";
            var resuultado = CalculoCubo.CalcularDatos(texto);
            Assert.AreEqual(resultadoEsperado, resuultado);
        }

        [TestMethod]
        public void ProcesoTestErrorMensaje()
        {
            string texto = "Test";
            var resultadoEsperado = "4\n4\n27\n0\n1\n1\n";
            var resuultado = CalculoCubo.CalcularDatos(texto);
            Assert.AreEqual(resultadoEsperado, resuultado);
        } 

    }
}
